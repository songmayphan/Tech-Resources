# Abstract Data Type

ADT is an abstraction of data structure with provides only the interface to which a data structure must adhere to

The interface does not give any specific details about how something should be implemented or in what programming language

Example: 

List > dynamic array, linked list

Queue > linked list based Queue, array based queue, Stack based queue

Map > Tree Map , Hash map/ Hash table

Vehicle > Golf cart, bicycle, smart car

# Computational Complexity 

## Analysis 

- How much time and space are needed 

# Big - O Notation

n - input size

Constant Time : 0(1)
Log Time 0(log(n))
Linear time O(N)
linearithmic Time: O(nlog(n))
Quadric Time: O(n^2)
cubic time : O(n^3)
exponential time: O(b^n)
Factorial time: O(n!)

ranked from best to worst
ignore constant value

O(2N) > O(N)

# Graph Algo

## Depth First Search 
-  The frontier is the list of nodes we know about, but have not visited. The explored list is the list of nodes that we’ve already visited. We use the frontier to track of which nodes will be explored next — the ordering of the frontier controls which search algorithm we’re performing. The explored list is used to prevent us from searching nodes we’ve already explored — and thus prevents us from searching in an infinite loop if there are cycles.

Pseudocode

`DFS(graph, start_node, end_node):
    frontier = new Stack()
    frontier.push(start_node)
    explored = new Set()
    while frontier is not empty:
        current_node = frontier.pop()
        if current_node in explored: continue
        if current_node == end_node: return success
        for neighbor in graph.get_neigbhors(current_node):
            frontier.push(neighbor)
        explored.add(current_node)`
## Breadth first search